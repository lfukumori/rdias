# RDIAS

## INFORMAÇÕES:

A aplicação é um gerenciador de catálogo de produto. A atualização e criação de produtos é feita através de uma planilha Excel.

O que vamos avaliar principalmente no seu teste:

 - Design do código
 - Padrão de código
 - Documentação
 - Pontualidade
 - Comprometimento
 - Se você atingiu o objetivo do produto a ser desenvolvido.
 - Avaliar seu conhecimento sobre APIs.
 - Avaliar a arquitetura e organização do código criado.
 - Se a sua solução foi simples de entender na visão de outro desenvolvedor.

Você deve mostrar o código fonte (não deve minificar o código), mostrar a estrutura antes da compilação, deve versionar seu projeto utilizando Git, utilizando preferencialmente BitBucket com um repositório publico.

### Objetivo do produto a ser entregue

Criar uma API RESTful que:

 - [x] Receberá uma planilha de produtos (segue em anexo) que deve ser processada em background (queue).
 - [x] Ter um endpoint que informe se a planilha for processada com sucesso ou não.
 - [x] Seja possível visualizar, atualizar e apagar os produtos (só é possível criar novos produtos via planilha).

Requisitos técnicos
 - [x] Laravel.
 - [x] Bitbucket.
 - [x] Utilizar Queue.
 - [x] REST.
 - [x] JSON.

### Histórias do usuário:

 - [x] A planilha de produtos (xlsx) deve ser enviada para um endpoint.
 - [x] O arquivo é armazenado em um local seguro.
 - [x] O arquivo é recebido pelo servidor via upload e retorna um id como referência.
 - [x] O arquivo é processado em uma fila em background (leitura dos produtos).
 - [x] O usuário pode consultar o status do processamento do arquivo.
 - [x] Uma consulta no processamento da fila retorna se conseguiu processar.
 - [x] Produtos podem ser listados, adicionados (via planilha), editados (via planilha) e removidos.

## Pré-requisitos necessários para instalação:

 - git
 - php
 - composer

## Instalação:

Clonar o repositório:

    git clone git@bitbucket.org:lfukumori/rdias.git

Copiar arquivo `.env`:

    cp .env.example .env

Instalar as dependências do projeto:

    composer install

Gerar chave para o projeto:

    php artisan key:gen

## Documentação:

 - [Laravel](laravel.md)
 - [Leia-me](readme.md)
 - [Endpoints](endpoint.md)

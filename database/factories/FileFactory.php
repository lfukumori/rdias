<?php

$factory->define(
    \App\Models\File::class,
    function (\Faker\Generator $faker) {
        return [
            'file' => \Illuminate\Http\UploadedFile::fake()
                ->create(
                    $faker->uuid.'.xlsx',
                    $faker->numberBetween(100, 1000),
                    'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
                ),
            'status' => \App\Models\File::STATUS_START,
        ];
    }
);

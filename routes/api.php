<?php

Route::prefix('v1')->name('v1.')->namespace('V1')->group(function () {
    Route::prefix('products')->name('product.')->namespace('Products')->group(function() {
        Route::prefix('upload')->name('upload.')->namespace('Upload')->group(function() {
            Route::post('', ['as' => 'store', 'uses' => StoreController::class]);
            Route::get('{model}', ['as' => 'show', 'uses' => ShowController::class]);
        });
        Route::get('', ['as' => 'index', 'uses' => IndexController::class]);
        Route::get('{model}', ['as' => 'show', 'uses' => ShowController::class]);
        Route::delete('{model}', ['as' => 'destroy', 'uses' => DestroyController::class]);
    });

});

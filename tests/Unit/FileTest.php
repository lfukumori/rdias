<?php

namespace Tests\Unit;

class FileTest extends \PHPUnit\Framework\TestCase
{
    /**
     * File Has Status Start.
     *
     * @return void
     */
    public function testFileHasStatusStart()
    {
        $this->assertEquals(0, \App\Models\File::STATUS_START);
    }

    /**
     * File Has Status On Queue.
     *
     * @return void
     */
    public function testFileHasStatusOnQueue()
    {
        $this->assertEquals(1, \App\Models\File::STATUS_ONQUEUE);
    }

    /**
     * File Has Status Success.
     *
     * @return void
     */
    public function testFileHasStatusSuccess()
    {
        $this->assertEquals(2, \App\Models\File::STATUS_SUCESS);
    }

    /**
     * File Has Status Fail.
     *
     * @return void
     */
    public function testFileHasStatusFail()
    {
        $this->assertEquals(99, \App\Models\File::STATUS_FAIL);
    }

    /**
     * File Has Statuses.
     *
     * @return void
     */
    public function testFileHasStatuses()
    {
        $this->assertEquals([
            \App\Models\File::STATUS_START,
            \App\Models\File::STATUS_ONQUEUE,
            \App\Models\File::STATUS_SUCESS,
            \App\Models\File::STATUS_FAIL,
        ], \App\Models\File::$statuses);
    }
}

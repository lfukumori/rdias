<?php

namespace Tests\Feature\File;

use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

class ValidateUploadFileTest extends \Tests\TestCase
{
    use \Illuminate\Foundation\Testing\RefreshDatabase;

    /**
     * Setup the test environment.
     *
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        // Create virtual products disk
        Storage::fake('products');
    }

    /**
     * A User Cannot Submit No File To An Endpoint.
     *
     * @return void
     */
    public function testAUserCannotSubmitNoFileToAnEndpoint()
    {
        // Submit file to endpoint
        $response = $this->json('POST', route('api.v1.product.upload.store'), [
        ]);
        // asset receive 422
        $response->assertStatus(422);
    }

    /**
     * A User Cannot Submit File As Field To An Endpoint.
     *
     * @return void
     */
    public function testAUserCannotSubmitFileAsFieldToAnEndpoint()
    {
        // Submit file to endpoint
        $response = $this->json('POST', route('api.v1.product.upload.store'), [
            'file' => 'this is a string',
        ]);
        // asset receive 422
        $response->assertStatus(422);
    }

    /**
     * A User Cannot Upload Another Format File To An Endpoint.
     *
     * @return void
     */
    public function testAUserCannotUploadAnotherFormatFileToAnEndpoint()
    {
        // Generate txt file
        $file = UploadedFile::fake()->create(
            'produto.txt',
            20,
            'text/plain'
        );
        // Submit file to endpoint
        $response = $this->json('POST', route('api.v1.product.upload.store'), [
            'file' => $file,
        ]);
        // asset receive 422
        $response->assertStatus(422);
        // Assert a file does not exist...
        Storage::disk('products')->assertMissing($file->hashName());
    }

    /**
     * A User Cannot Submit A Large File To An Endpoint.
     *
     * @return void
     */
    public function testAUserCannotSubmitALargeFileToAnEndpoint()
    {
        // Generate upload file
        $file = UploadedFile::fake()->create(
            'produto.xlsx',
            20000,
            'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
        );
        // Submit file to endpoint
        $response = $this->json('POST', route('api.v1.product.upload.store'), [
            'file' => $file,
        ]);
        // asset receive 422
        $response->assertStatus(422);
        // Assert a file does not exist...
        Storage::disk('products')->assertMissing($file->hashName());
    }

    /**
     * A User Cannot Submit Empty File To An Endpoint.
     *
     * @return void
     */
    public function testAUserCannotSubmitEmptyFileToAnEndpoint()
    {
        // Generate upload file
        $file = UploadedFile::fake()->create(
            'produto.xlsx',
            0,
            'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
        );
        // Submit file to endpoint
        $response = $this->json('POST', route('api.v1.product.upload.store'), [
            'file' => $file,
        ]);
        // asset receive 422
        $response->assertStatus(422);
        // Assert a file does not exist...
        Storage::disk('products')->assertMissing($file->hashName());
    }
}

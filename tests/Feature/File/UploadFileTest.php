<?php

namespace Tests\Feature\File;

use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

class UploadFileTest extends \Tests\TestCase
{
    use \Illuminate\Foundation\Testing\RefreshDatabase;

    /**
     * Setup the test environment.
     *
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        // Create virtual products disk
        Storage::fake('products');
    }

    /**
     * A User Can Upload A Microsoft Excel File To An Endpoint.
     *
     * @return void
     */
    public function testAUserCanUploadAMicrosoftExcelFileToAnEndpoint()
    {
        // Generate upload file
        $file = UploadedFile::fake()->create(
            'produto.xlsx',
            20,
            'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
        );
        // Submit file to endpoint
        $response = $this->json('POST', route('api.v1.product.upload.store'), [
            'file' => $file,
        ]);

        // asset receive 201
        $response->assertStatus(201);
        // Assert the file was stored
        Storage::disk('products')->assertExists($file->hashName());
    }

    /**
     * A User Can Upload A Spreadsheet File To An Endpoint.
     *
     * @return void
     */
    public function testAUserCanUploadASpreadsheetFileToAnEndpoint()
    {
        // Generate upload file
        $file = UploadedFile::fake()->create(
            'produto.ods',
            20,
            'application/vnd.oasis.opendocument.spreadsheet'
        );
        // Submit file to endpoint
        $response = $this->json('POST', route('api.v1.product.upload.store'), [
            'file' => $file,
        ]);
        // asset receive 201
        $response->assertStatus(201);
        // Assert the file was stored
        Storage::disk('products')->assertExists($file->hashName());
    }
}

<?php

namespace Tests\Feature\File;

use Illuminate\Support\Facades\Storage;

class GenerateIdAfterUploadFileTest extends \Tests\TestCase
{
    use \Illuminate\Foundation\Testing\RefreshDatabase;

    /**
     * Setup the test environment.
     *
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        // Create virtual products disk
        Storage::fake('products');
    }

    /**
     * A User Receive Id When Upload File To An Endpoint.
     *
     * @return void
     */
    public function testAUserReceiveIdWhenUploadFileToAnEndpoint()
    {
        // Generate model file
        $file = factory(\App\Models\File::class)->make();
        // Submit file to endpoint
        $response = $this->json('POST', route('api.v1.product.upload.store'), [
            'file' => $file->file,
        ]);
        // get model from database
        $model = \App\Models\File::where('file', $file->file->hashName())
            ->where('status', $file->status)
            ->first();
        // Assert see right response
        $response->assertStatus(201)
            ->assertSee('id', $model->id)
            ->assertSee('status', 'success');
        // Assert file into database
        $this->assertDatabaseHas('files', [
            'file' => $file->file->hashName(),
            'status' => $file->status,
        ]);
    }
}

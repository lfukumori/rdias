<?php

namespace Tests\Feature\File;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Queue;

class JobProcessedAfterDispatchedTest extends \Tests\TestCase
{
    use \Illuminate\Foundation\Testing\RefreshDatabase;

    /**
     * Setup the test environment.
     *
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        // Create virtual products disk
        Storage::fake('products');
    }

    /**
     * A User Receive Id When Upload File To An Endpoint.
     *
     * @return void
     */
    public function testAUserReceiveIdWhenUploadFileToAnEndpoint()
    {
        $this->withoutExceptionHandling();
        // Fake Queue
        Queue::fake();
        // Assert that no jobs were pushed...
        Queue::assertNothingPushed();
        // Generate model file
        $file = factory(\App\Models\File::class)->make();
        // Submit file to endpoint
        $this->json('POST', route('api.v1.product.upload.store'), [
            'file' => $file->file,
        ]);
        // get model from database
        $model = \App\Models\File::where('file', $file->file->hashName())
            ->where('status', $file->status)
            ->first();
        // Assert the job was pushed to the queue
        Queue::assertPushed(
            \App\Jobs\Api\V1\Products\Upload\StoreJob::class,
            function ($job) use ($model) {
                return $job->file->id === $model->id;
            }
        );
    }
}

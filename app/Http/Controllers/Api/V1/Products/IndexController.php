<?php

namespace App\Http\Controllers\Api\V1\Products;

class IndexController extends \App\Http\Controllers\Controller
{
    /**
     * Procucts Index.
     *
     * @throws \Exception
     *
     * @return \Illuminate\Http\Response
     */
    public function __invoke()
    {
        try {
            // Return nice Response
            return \App\Http\Resources\Api\V1\Products\ProductResource::collection(
                \App\Models\Product::paginate()
            );
        } catch (\Exception $exception) {
            throw $exception;
        }
    }
}

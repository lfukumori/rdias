<?php

namespace App\Http\Controllers\Api\V1\Products\Upload;

class ShowController extends \App\Http\Controllers\Controller
{
    /**
     * Show Store.
     *
     * @param  \App\Models\File $model
     *
     * @throws \Exception
     *
     * @return \Illuminate\Http\Response
     */
    public function __invoke(\App\Models\File $model)
    {
        try {
            // Return nice Response
            return new \App\Http\Resources\Api\V1\Products\Upload\ShowResource($model);
        } catch (\Exception $exception) {
            throw $exception;
        }
    }
}

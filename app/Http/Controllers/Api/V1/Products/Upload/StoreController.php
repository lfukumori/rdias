<?php

namespace App\Http\Controllers\Api\V1\Products\Upload;

class StoreController extends \App\Http\Controllers\Controller
{
    /**
     * Upload Store.
     *
     * @param  \App\Http\Requests\Api\V1\Products\Upload\StoreRequest $request
     *
     * @throws \Exception
     *
     * @return \Illuminate\Http\Response
     */
    public function __invoke(\App\Http\Requests\Api\V1\Products\Upload\StoreRequest $request)
    {
        try {
            // Store file into disk products
            $file = $request->file('file')->store('', 'products');
            // Add file into a database
            $model = \App\Models\File::create([
                'file' => $file,
            ]);
            // Dispatch model into job
            \App\Jobs\Api\V1\Products\Upload\StoreJob::dispatch($model);
            // Return nice Response
            return new \App\Http\Resources\Api\V1\Products\Upload\StoreResource($model);
        } catch (\Exception $exception) {
            throw $exception;
        }
    }
}

<?php

namespace App\Http\Controllers\Api\V1\Products;

class DestroyController extends \App\Http\Controllers\Controller
{
    /**
     * Procucts Index.
     *
     * @throws \Exception
     *
     * @return \Illuminate\Http\Response
     */
    public function __invoke(\App\Models\Product $model)
    {
        try {
            // clone model
            $clone =& $model;
            // delete entity
            $model->delete();
            // Return nice Response
            return (new \App\Http\Resources\Api\V1\Products\DestroyResource(
                $clone
            ))->response()->setStatusCode(202);
        } catch (\Exception $exception) {
            throw $exception;
        }
    }
}

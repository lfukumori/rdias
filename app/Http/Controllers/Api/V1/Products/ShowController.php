<?php

namespace App\Http\Controllers\Api\V1\Products;

class ShowController extends \App\Http\Controllers\Controller
{
    /**
     * Procucts Index.
     *
     * @throws \Exception
     *
     * @return \Illuminate\Http\Response
     */
    public function __invoke(\App\Models\Product $model)
    {
        try {
            // Return nice Response
            return new \App\Http\Resources\Api\V1\Products\ProductResource(
                $model
            );
        } catch (\Exception $exception) {
            throw $exception;
        }
    }
}

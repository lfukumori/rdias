<?php

namespace App\Http\Resources\Api\V1\Products\Upload;

class ShowResource extends \Illuminate\Http\Resources\Json\JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->resource->id,
            'status' => $this->showStatus(),
        ];
    }

    /**
     * Get additional data that should be returned with the resource array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function with($request)
    {
        return [
            'success' => true,
            'status' => 200,
        ];
    }

    public function showStatus()
    {
        switch ($this->resource->status) {
            case \App\Models\File::STATUS_START:
                return 'A ser processado.';
            case \App\Models\File::STATUS_ONQUEUE:
                return 'Em processamento.';
            case \App\Models\File::STATUS_SUCESS:
                return 'Processado com sucesso.';
            default:
            case \App\Models\File::STATUS_FAIL:
                return 'Erro ao processar.';
        }
    }
}

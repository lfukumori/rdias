<?php

namespace App\Jobs\Api\V1\Products\Upload;

class StoreJob implements \Illuminate\Contracts\Queue\ShouldQueue
{
    use \Illuminate\Bus\Queueable;
    use \Illuminate\Foundation\Bus\Dispatchable;
    use \Illuminate\Queue\InteractsWithQueue;
    use \Illuminate\Queue\SerializesModels;

    public $file;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(\App\Models\File $file)
    {
        $this->file = $file;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        \Maatwebsite\Excel\Facades\Excel::import(
            new \App\Imports\ProductsImport($this->file),
            $this->file->file,
            'products'
        );
    }
}

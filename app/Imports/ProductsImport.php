<?php

namespace App\Imports;

class ProductsImport implements
    \Illuminate\Contracts\Queue\ShouldQueue,
    \Maatwebsite\Excel\Concerns\ToModel,
    \Maatwebsite\Excel\Concerns\WithChunkReading,
    \Maatwebsite\Excel\Concerns\WithEvents,
    \Maatwebsite\Excel\Concerns\WithStartRow
{
    use \Maatwebsite\Excel\Concerns\RegistersEventListeners;
    /**
     * @var \App\Models\File
     */
    public $file;

    /**
     * Constructor.
     *
     * @param \App\Models\File $file
     */
    public function __construct(\App\Models\File $file)
    {
        $this->file = $file;
    }

    /**
     * Import into eloquent model.
     *
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function model(array $row)
    {
        return new \App\Models\Product([
            'id' => $row[0],
            'name' => $row[1],
            'quantity' => $row[2],
        ]);
    }

    /**
     * Chunk size.
     *
     * @return int
     */
    public function chunkSize(): int
    {
        return 100;
    }

    /**
     * Start row.
     *
     * @return int
     */
    public function startRow(): int
    {
        return 2;
    }

    public function changeStatus(int $status): int
    {
        return $this->file->setStatus($status);
    }

    public static function beforeImport(\Maatwebsite\Excel\Events\BeforeImport $event)
    {
        $event->getConcernable()->changeStatus(\App\Models\File::STATUS_ONQUEUE);
    }

    public static function afterImport(\Maatwebsite\Excel\Events\AfterImport $event)
    {
        $event->getConcernable()->changeStatus(\App\Models\File::STATUS_SUCESS);
    }

    public static function importFailed(\Maatwebsite\Excel\Events\ImportFailed $event)
    {
        $event->getConcernable()->changeStatus(\App\Models\File::STATUS_FAIL);
    }
}

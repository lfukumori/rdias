<?php

namespace App\Models;

use Illuminate\Database\Eloquent\JsonEncodingException;
use Illuminate\Database\Eloquent\MassAssignmentException;

class File extends \Illuminate\Database\Eloquent\Model
{
    const STATUS_START = 0;
    const STATUS_ONQUEUE = 1;
    const STATUS_SUCESS = 2;
    const STATUS_FAIL = 99;

    /**
     * Array of statuses.
     *
     * @var array
     */
    static public $statuses = [
        self::STATUS_START,
        self::STATUS_ONQUEUE,
        self::STATUS_SUCESS,
        self::STATUS_FAIL,
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'file',
        'status',
    ];

    /**
     * Update status.
     *
     * @param int $status
     *
     * @return int
     */
    public function setStatus(int $status)
    {
        if (in_array($status, self::$statuses)) {
            $this->update(['status' => $status]);
        }

        return $this->attributes['status'];
    }
}

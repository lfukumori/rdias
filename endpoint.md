# ENDPOINTS

**Upload do arquivo de produto**
----
  Envia uma planilha de produtos a ser processado.

* **URL**

  /api/v1/products/upload

* **Method:**

  `POST`

* **URL Params**

  Nenhum

* **Data Params**

  **Required:** <br />
  `file=[file]`

* **Success Response:**

  * **Code:** 201 <br />
    **Content:** `{
    "data": {
        "id": 1
    },
    "success": true,
    "status": 201
    }`

**Consulta do status de processamento do arquivo**
----
  Verifica o status de processamento da planilha.

* **URL**

  /api/v1/products/upload/:id

* **Method:**

  `GET`

* **URL Params**

  **Required:** <br />
  `id=[integer]`

* **Data Params**

  Nenhum

* **Success Response:**

  * **Code:** 200 <br />
    **Content:** `{
    "data": {
        "id": 1,
        "status": "Processado com sucesso."
    },
    "success": true,
    "status": 200
}`

**Listagem de produtos**
----
  Listagem de todos os produtos paginado.

* **URL**

  /api/v1/products

* **Method:**

  `GET`

* **URL Params**

  **Optional:** <br />
  `page=[integer]`
  `per_page=[integer]`


* **Data Params**

  Nenhum

* **Success Response:**

  * **Code:** 200 <br />
    **Content:** `{
        "data": [
            {
                "id": 1,
                "name": "teste1",
                "quantity": 1
            },
            {
                "id": 2,
                "name": "teste2",
                "quantity": 2
            },
            {
                "id": 3,
                "name": "teste3",
                "quantity": 3
            },
            {
                "id": 4,
                "name": "teste4",
                "quantity": 4
            },
            {
                "id": 5,
                "name": "teste5",
                "quantity": 5
            },
            {
                "id": 6,
                "name": "teste6",
                "quantity": 6
            },
            {
                "id": 7,
                "name": "teste7",
                "quantity": 7
            },
            {
                "id": 8,
                "name": "teste8",
                "quantity": 8
            },
            {
                "id": 9,
                "name": "teste9",
                "quantity": 9
            },
            {
                "id": 10,
                "name": "teste10",
                "quantity": 10
            },
            {
                "id": 11,
                "name": "teste11",
                "quantity": 11
            },
            {
                "id": 12,
                "name": "teste12",
                "quantity": 12
            },
            {
                "id": 13,
                "name": "teste13",
                "quantity": 13
            },
            {
                "id": 14,
                "name": "teste14",
                "quantity": 14
            },
            {
                "id": 15,
                "name": "teste15",
                "quantity": 15
            }
        ],
        "links": {
            "first": "/api/v1/products?page=1",
            "last": "/api/v1/products?page=13",
            "prev": null,
            "next": "/api/v1/products?page=2"
        },
        "meta": {
            "current_page": 1,
            "from": 1,
            "last_page": 13,
            "path": "/api/v1/products",
            "per_page": 15,
            "to": 15,
            "total": 189
        }
    }`

**Listagem do produto**
----
  Exibe um produto em específico

* **URL**

  /api/v1/products/:id

* **Method:**

  `GET`

* **URL Params**

  **Required:** <br />
  `id=[integer]`

* **Data Params**

  Nenhum

* **Success Response:**

  * **Code:** 200 <br />
    **Content:** `{
        "data": {
            "id": 1,
            "name": "teste1",
            "quantity": 1
        },
        "success": true,
        "status": 200
    }`

**Remoção do produto**
----
  Remove um produto específico

* **URL**

  /api/v1/products/:id

* **Method:**

  `DELETE`

* **URL Params**

  **Required:** <br />
  `id=[integer]`

* **Data Params**

  Nenhum

* **Success Response:**

  * **Code:** 202 <br />
    **Content:** `{
        "data": {
            "id": 1
        },
        "success": true,
        "status": 202
    }`

## Documentação:

 - [Laravel](laravel.md)
 - [Leia-me](readme.md)
 - [Endpoints](endpoint.md)
